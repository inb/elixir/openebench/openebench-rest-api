###### OpenEBench GraphQL API
OpenEBench GraphQL API is based on the [Microprofile GraphQL Specification](https://github.com/eclipse/microprofile-graphql).  
The API definition is found at [https://openebench.bsc.es/api/scientific/graphql/schema.graphql](https://openebench.bsc.es/api/scientific/graphql/schema.graphql).  

Example:  

>**curl.exe -u user:password -H "Content-type: application/json" https://openebench.bsc.es/api/scientific/graphql -d @[get_communities_query.json](./examples/get_communities_query.json)**

