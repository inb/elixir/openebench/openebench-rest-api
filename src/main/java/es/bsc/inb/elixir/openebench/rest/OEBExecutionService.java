/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.rest;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import es.bsc.inb.elixir.openebench.auth.OEBRoles;
import es.bsc.inb.elixir.openebench.mongo.migration.Migrator;
import es.bsc.inb.elixir.openebench.validator.OpenEBenchSchemaResolver;
import java.io.IOException;
import java.io.StringWriter;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.security.PermitAll;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * @author Dmitry Repchevsky
 */

@Path("/execute")
@ApplicationScoped
public class OEBExecutionService {

    @Inject 
    private ServletContext ctx;

    private OpenEBenchSchemaResolver schema_resolver;
    
    private ConnectionString staged_uri;
    private ConnectionString sandbox_uri;
    
    private MongoClient staged_mc;
    private MongoClient sandbox_mc;

    @PostConstruct
    protected void init() {
        schema_resolver = new OpenEBenchSchemaResolver(ctx.getInitParameter("json.schemas.uri"));
        
        staged_uri = new ConnectionString(ctx.getInitParameter("mongodb.staged.url"));
        sandbox_uri = new ConnectionString(ctx.getInitParameter("mongodb.sandbox.url"));
        
        staged_mc = MongoClients.create(staged_uri);
        sandbox_mc = MongoClients.create(sandbox_uri);
    }
    
    @PreDestroy
    public void destroy() {
        staged_mc.close();
        sandbox_mc.close();
    }
    
    @GET
    @Path("/migrate")
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public Response doMigrateData(@QueryParam("dryrun") String dryrun,
                                  @Context SecurityContext ctx) {
        Migrator tool = new Migrator(
                staged_mc.getDatabase(staged_uri.getDatabase()),
                sandbox_mc.getDatabase(sandbox_uri.getDatabase()),
                schema_resolver,
                ctx.getUserPrincipal().getName(),
                ctx.isUserInRole(OEBRoles.ADMIN), // keep_prov
                !"false".equals(dryrun)
        );
        
        String result;
        final StringWriter err = new StringWriter();
        try (err) {
            result = tool.migrate(err);
        } catch (IOException ex) {
            result = null;
        }        
        
        return result != null ? Response.ok(result, MediaType.APPLICATION_JSON).build() : Response.status(409).entity(err.toString()).build();
    }

}
