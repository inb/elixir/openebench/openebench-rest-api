/**
 * *****************************************************************************
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql;

import com.mongodb.client.MongoCollection;
import es.bsc.inb.elixir.openebench.auth.Authorization;
import es.bsc.inb.elixir.openebench.auth.CollectionAuthorizationFilter;
import es.bsc.inb.elixir.openebench.graphql.inputs.BenchmarkingEventFilter;
import es.bsc.inb.elixir.openebench.graphql.inputs.ChallengeFilter;
import es.bsc.inb.elixir.openebench.graphql.inputs.CommunityFilter;
import es.bsc.inb.elixir.openebench.graphql.inputs.ContactFilter;
import es.bsc.inb.elixir.openebench.graphql.inputs.DatasetFilter;
import es.bsc.inb.elixir.openebench.graphql.inputs.MetricsFilter;
import es.bsc.inb.elixir.openebench.graphql.inputs.TestActionFilter;
import es.bsc.inb.elixir.openebench.graphql.inputs.ToolFilter;
import es.bsc.inb.elixir.openebench.graphql.model.BenchmarkingEvent;
import es.bsc.inb.elixir.openebench.graphql.model.Challenge;
import es.bsc.inb.elixir.openebench.graphql.model.Community;
import es.bsc.inb.elixir.openebench.graphql.model.Contact;
import es.bsc.inb.elixir.openebench.graphql.model.Dataset;
import es.bsc.inb.elixir.openebench.graphql.model.Metrics;
import es.bsc.inb.elixir.openebench.graphql.model.TestAction;
import es.bsc.inb.elixir.openebench.graphql.model.Tool;
import es.bsc.inb.elixir.openebench.graphql.model.ToolMetrics;
import es.bsc.inb.elixir.openebench.mongo.MongoConnection;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchCollection;
import es.bsc.inb.elixir.openebench.security.OEBSecurityContext;
import es.bsc.inb.elixir.openebench.validator.MongoFilteredCollectionFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.security.PermitAll;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonReader;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import org.bson.Document;
import org.bson.json.JsonWriterSettings;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Name;
import org.eclipse.microprofile.graphql.Query;
import org.eclipse.microprofile.graphql.Source;

/**
 * @author Dmitry Repchevsky
 */

@GraphQLApi
@RequestScoped
public class OEBGraphQLService {
    
    @Inject
    private OEBSecurityContext ctx;

    @Inject
    private MongoConnection mongo;
    
    private JsonWriterSettings settings;
    private Authorization authorization;
    
    @PostConstruct
    public void init() {
        settings = JsonWriterSettings.builder().
                dateTimeConverter(new ISO8601DateTimeConverter()).build();
        authorization = new Authorization(mongo.getStagedDatabase(), null);
    }

    @PermitAll
    @Query("getContacts")
    public List<Contact> getContacts(@Name("contactFilters") ContactFilter filters) {
        final Jsonb jsonb = JsonbBuilder.create();
        final List<Contact> contacts = new ArrayList();
        try (JsonReader reader = filters == null ? null : Json.createReader(new StringReader(jsonb.toJson(filters)))) {
            final MongoCollection<Document> collection = mongo.getStagedDatabase().getCollection(OpenEBenchCollection.CONTACT);
            final CollectionAuthorizationFilter iterator = MongoFilteredCollectionFactory.getCollectionIterator(collection, 
                        reader == null ? null : reader.readObject(), 
                        authorization.getAuthorization(ctx, OpenEBenchCollection.CONTACT, false));

            if (iterator != null) {
                try (iterator) {
                    while (iterator.hasNext()) {
                        final Document contact_document = iterator.next();
                        if (contact_document != null) {
                            final Contact contact = jsonb.fromJson(contact_document
                                    .toJson(settings), Contact.class);
                            contacts.add(contact);
                        }
                    }
                }
            }
        }
        return contacts;
    }
    
    @PermitAll
    @Query("getCommunities")
    public List<Community> getCommunities(@Name("communityFilters") CommunityFilter filters) {
        final Jsonb jsonb = JsonbBuilder.create();
        final List<Community> communities = new ArrayList();
        try (JsonReader reader = filters == null ? null : Json.createReader(new StringReader(jsonb.toJson(filters)))) {
            final MongoCollection<Document> collection = mongo.getStagedDatabase().getCollection(OpenEBenchCollection.COMMUNITY);
            final CollectionAuthorizationFilter iterator = MongoFilteredCollectionFactory.getCollectionIterator(collection, 
                        reader == null ? null : reader.readObject(), 
                        authorization.getAuthorization(ctx, OpenEBenchCollection.COMMUNITY, false));
            if (iterator != null) {
                try (iterator) {
                    while (iterator.hasNext()) {
                        final Document community_document = iterator.next();
                        if (community_document != null) {
                            final Community community = jsonb.fromJson(
                                    community_document.toJson(settings), Community.class);
                            communities.add(community);
                        }
                    }
                }
            }
        }
        return communities;
    }

    @Query("benchmarkingEvents")
    public List<BenchmarkingEvent> getBenchmarkingEvents(@Source Community community, @Name("benchmarkingEventFilters") BenchmarkingEventFilter filters) {
        if (filters == null) {
            filters = new BenchmarkingEventFilter();
        }
        filters.setCommunityId(community.getID());
        return getBenchmarkingEvents(filters);
    }

    @PermitAll
    @Query("getBenchmarkingEvents")
    public List<BenchmarkingEvent> getBenchmarkingEvents(@Name("benchmarkingEventFilters") BenchmarkingEventFilter filters) {
        final Jsonb jsonb = JsonbBuilder.create();
        final List<BenchmarkingEvent> benchmarking_events = new ArrayList();
        try (JsonReader reader = filters == null ? null : Json.createReader(new StringReader(jsonb.toJson(filters)))) {
            final MongoCollection<Document> collection = mongo.getStagedDatabase().getCollection(OpenEBenchCollection.BENCHMARKING_EVENT);
            final CollectionAuthorizationFilter iterator = MongoFilteredCollectionFactory.getCollectionIterator(collection, 
                        reader == null ? null : reader.readObject(), 
                        authorization.getAuthorization(ctx, OpenEBenchCollection.BENCHMARKING_EVENT, false));
            if (iterator != null) {
                try (iterator) {
                    while (iterator.hasNext()) {
                        final Document benchmarking_event_document = iterator.next();
                        if (benchmarking_event_document != null) {
                            final Document _id = benchmarking_event_document.get("_id", Document.class);
                            if (_id != null) {
                                benchmarking_event_document.put("_id", _id.getString("id"));
                                benchmarking_event_document.put("_revision", _id.getInteger("revision"));
                                final BenchmarkingEvent benchmarking_event = jsonb.fromJson(
                                        benchmarking_event_document.toJson(settings), BenchmarkingEvent.class);
                                benchmarking_events.add(benchmarking_event);
                            }
                        }
                    }                    
                }
            }
        }
        return benchmarking_events;
    }

    @Query("challenges")
    public List<Challenge> getBenchmarkingEvents(@Source BenchmarkingEvent benchmarking_event, @Name("challengeFilters") ChallengeFilter filters) {
        if (filters == null) {
            filters = new ChallengeFilter();
        }
        filters.setBenchmarkingEventId(benchmarking_event.getID());
        return getChallenges(filters);
    }

    @PermitAll
    @Query("getChallenges")
    public List<Challenge> getChallenges(@Name("challengeFilters") ChallengeFilter filters) {
        final Jsonb jsonb = JsonbBuilder.create();
        final List<Challenge> challenges = new ArrayList();
        try (JsonReader reader = filters == null ? null : Json.createReader(new StringReader(jsonb.toJson(filters)))) {
            final MongoCollection<Document> collection = mongo.getStagedDatabase().getCollection(OpenEBenchCollection.CHALLENGE);
            final CollectionAuthorizationFilter iterator = MongoFilteredCollectionFactory.getCollectionIterator(collection, 
                        reader == null ? null : reader.readObject(), 
                        authorization.getAuthorization(ctx, OpenEBenchCollection.CHALLENGE, false));
            if (iterator != null) {
                try (iterator) {
                    while (iterator.hasNext()) {
                        final Document challenge_document = iterator.next();
                        if (challenge_document != null) {
                            final Document _id = challenge_document.get("_id", Document.class);
                            if (_id != null) {
                                challenge_document.put("_id", _id.getString("id"));
                                challenge_document.put("_revision", _id.getInteger("revision"));
                                final Challenge challenge = jsonb.fromJson(
                                        challenge_document.toJson(settings), Challenge.class);
                                challenges.add(challenge);
                            }
                        }
                    }                    
                }
            }
        }
        return challenges;
    }
    
    @PermitAll
    @Query("getTools")
    public List<Tool> getTools(@Name("toolFilters") ToolFilter filters) {
        final Jsonb jsonb = JsonbBuilder.create();
        final List<Tool> tools = new ArrayList();
        try (JsonReader reader = filters == null ? null : Json.createReader(new StringReader(jsonb.toJson(filters)))) {
            final MongoCollection<Document> collection = mongo.getStagedDatabase().getCollection(OpenEBenchCollection.TOOL);
            final CollectionAuthorizationFilter iterator = MongoFilteredCollectionFactory.getCollectionIterator(collection, 
                        reader == null ? null : reader.readObject(), 
                        authorization.getAuthorization(ctx, OpenEBenchCollection.TOOL, false));

            if (iterator != null) {
                try (iterator) {
                    while (iterator.hasNext()) {
                        final Document tool_document = iterator.next();
                        if (tool_document != null) {
                            final Document _id = tool_document.get("_id", Document.class);
                            if (_id != null) {
                                tool_document.put("_id", _id.getString("id"));
                                tool_document.put("_revision", _id.getInteger("revision"));
                                final Tool tool = jsonb.fromJson(tool_document.toJson(settings), Tool.class);
                                tools.add(tool);
                            }
                        }
                    }                    
                }
            }
        }
        return tools;
    }
    
    @Query("datasets")
    public List<Dataset> getDatasets(@Source Challenge challenge, @Name("datasetFilters") DatasetFilter filters) {
        if (filters == null) {
            filters = new DatasetFilter();
        }
        filters.setChallengeId(challenge.getID());
        return getDatasets(filters);
    }

    @PermitAll
    @Query("getDatasets")
    public List<Dataset> getDatasets(@Name("datasetFilters") DatasetFilter filters) {
        final Jsonb jsonb = JsonbBuilder.create();
        final List<Dataset> datasets = new ArrayList();
        try (JsonReader reader = filters == null ? null : Json.createReader(new StringReader(jsonb.toJson(filters)))) {
            final MongoCollection<Document> collection = mongo.getStagedDatabase().getCollection(OpenEBenchCollection.DATASET);
            final CollectionAuthorizationFilter iterator = MongoFilteredCollectionFactory.getCollectionIterator(collection, 
                        reader == null ? null : reader.readObject(), 
                        authorization.getAuthorization(ctx, OpenEBenchCollection.DATASET, false));
            if (iterator != null) {
                try (iterator) {
                    while (iterator.hasNext()) {
                        final Document dataset_document = iterator.next();
                        if (dataset_document != null) {
                            final Document _id = dataset_document.get("_id", Document.class);
                            if (_id != null) {
                                dataset_document.put("_id", _id.getString("id"));
                                dataset_document.put("_revision", _id.getInteger("revision"));
                                final Dataset dataset = jsonb.fromJson(
                                        dataset_document.toJson(settings), Dataset.class);
                                datasets.add(dataset);
                            }
                        }
                    }                    
                }
            }
        }
        return datasets;
    }

    @Query("orig_id")
    public String getOriginalID(@Source ToolMetrics tool_metrics) {
        final MetricsFilter filters = new MetricsFilter();
        filters.setId(tool_metrics.getMetricsID());
        List<Metrics> metrics = getMetrics(filters);
        return metrics == null || metrics.isEmpty() ? null : metrics.get(0).getOriginalID();
    }

    @PermitAll
    @Query("getMetrics")
    public List<Metrics> getMetrics(@Name("metricsFilters") MetricsFilter filters) {
        final Jsonb jsonb = JsonbBuilder.create();
        final List<Metrics> metrics = new ArrayList();
        try (JsonReader reader = filters == null ? null : Json.createReader(new StringReader(jsonb.toJson(filters)))) {
            final MongoCollection<Document> collection = mongo.getStagedDatabase().getCollection(OpenEBenchCollection.METRICS);
            final CollectionAuthorizationFilter iterator = MongoFilteredCollectionFactory.getCollectionIterator(collection, 
                        reader == null ? null : reader.readObject(), 
                        authorization.getAuthorization(ctx, OpenEBenchCollection.METRICS, false));
            if (iterator != null) {
                try (iterator) {
                    while (iterator.hasNext()) {
                        final Document metrics_document = iterator.next();
                        if (metrics_document != null) {
                            final Document _id = metrics_document.get("_id", Document.class);
                            if (_id != null) {
                                metrics_document.put("_id", _id.getString("id"));
                                metrics_document.put("_revision", _id.getInteger("revision"));
                                final Metrics metric = jsonb.fromJson(
                                        metrics_document.toJson(settings), Metrics.class);
                                metrics.add(metric);
                            }
                        }
                    }                    
                }
            }
        }
        return metrics;
    }
    
    @Query("test_actions")
    public List<TestAction> getTestActions(@Source Challenge challenge, @Name("testActionFilters") TestActionFilter filters) {
        if (filters == null) {
            filters = new TestActionFilter();
        }
        filters.setChallengeID(challenge.getID());
        return getTestActions(filters);
    }

    @PermitAll
    @Query("getTestActions")
    public List<TestAction> getTestActions(@Name("testActionFilters") TestActionFilter filters) {
        final Jsonb jsonb = JsonbBuilder.create();
        final List<TestAction> test_actions = new ArrayList();
        try (JsonReader reader = filters == null ? null : Json.createReader(new StringReader(jsonb.toJson(filters)))) {
            final MongoCollection<Document> collection = mongo.getStagedDatabase().getCollection(OpenEBenchCollection.TEST_ACTION);
            final CollectionAuthorizationFilter iterator = MongoFilteredCollectionFactory.getCollectionIterator(collection, 
                        reader == null ? null : reader.readObject(), 
                        authorization.getAuthorization(ctx, OpenEBenchCollection.TEST_ACTION, false));
            if (iterator != null) {
                try (iterator) {
                    while (iterator.hasNext()) {
                        final Document test_action_document = iterator.next();
                        if (test_action_document != null) {
                            final Document _id = test_action_document.get("_id", Document.class);
                            if (_id != null) {
                                test_action_document.put("_id", _id.getString("id"));
                                test_action_document.put("_revision", _id.getInteger("revision"));
                                final TestAction test_action = jsonb.fromJson(
                                        test_action_document.toJson(settings), TestAction.class);
                                test_actions.add(test_action);
                            }
                        }
                    }                    
                }
            }
        }
        return test_actions;
    }
}
