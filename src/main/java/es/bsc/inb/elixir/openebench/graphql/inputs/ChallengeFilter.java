/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.inputs;

import es.bsc.inb.elixir.openebench.graphql.CompositeID;
import javax.json.bind.annotation.JsonbProperty;
import org.eclipse.microprofile.graphql.Input;

/**
 * @author Dmitry Repchevsky
 */

@Input("ChallengeFilters")
public class ChallengeFilter {
    private String id;
    private String acronym;
    private String benchmarking_event_id;

    public ChallengeFilter() {}

    @JsonbProperty("_id")
    public CompositeID getID() {
        return id == null ? null : new CompositeID(id);
    }

    @JsonbProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonbProperty("acronym")
    public String getAcronym() {
        return acronym;
    }

    @JsonbProperty("acronym")
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    @JsonbProperty("benchmarking_event_id")
    public String getBenchmarkingEventId() {
        return benchmarking_event_id;
    }

    @JsonbProperty("benchmarking_event_id")
    public void setBenchmarkingEventId(String benchmarking_event_id) {
        this.benchmarking_event_id = benchmarking_event_id;
    }

}
