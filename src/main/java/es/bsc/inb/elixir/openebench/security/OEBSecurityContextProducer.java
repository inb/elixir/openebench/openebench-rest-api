/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.security;

import java.security.Principal;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.SecurityContext;

import org.wildfly.security.http.oidc.OidcPrincipal;

/**
 * @author Dmitry Repchevsky
 */

@Named
@RequestScoped
public class OEBSecurityContextProducer {
    
    @Inject
    private HttpServletRequest req;
    private OEBSecurityContext ctx;

    @PostConstruct
    public void init() {
        final Principal principal = req.getUserPrincipal();
        if (principal instanceof OidcPrincipal) {
            ctx = new OEBSecurityContext((OidcPrincipal)principal);
        } else {
            ctx = new OEBSecurityContext((SecurityContext)null);
        }
    }

    @Produces
    public OEBSecurityContext mongoClient() {
        return ctx;
    }
}
