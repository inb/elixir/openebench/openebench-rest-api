/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.rest;

import es.bsc.inb.elixir.openebench.mongo.OpenEBenchCollection;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchID;
import es.bsc.inb.elixir.openebench.security.OEBSecurityContext;
import es.bsc.inb.elixir.openebench.validator.ValidationException;
import es.bsc.inb.elixir.openebench.validator.FKValidationError;
import es.elixir.bsc.json.schema.ValidationError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.annotation.security.PermitAll;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.StreamingOutput;

/**
 * OpenEBench 'GET', 'PUT', 'POST', 'PATCH' implementation 
 * (shared between 'staged' and 'sandbox' endpoints).
 * 
 * @author Dmitry Repchevsky
 */

public abstract class AbstractOEBProtectedService extends AbstractOEBPublicService {

    @PUT
    @Operation(
        summary = "Put the object into the database",
        responses = {
            @ApiResponse(responseCode = "201", description = "object has been put"),
            @ApiResponse(responseCode = "400", description = "validation error")
        }
    )
    @PermitAll
    @Path("/{collection}/{id : .*}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addObject(@PathParam("collection")
                              @Parameter(description = "data collection",
                                         example = "BenchmarkingEvent")
                              String collection, 
                              @PathParam("id") 
                              @Parameter(description = "object id",
                                         example = "CAMEO-3D:27")
                              String id,
                              @QueryParam("community_id")
                              @Parameter(description = "community id",
                                         example = "OEBC004")
                              String community_id,
                              @Parameter(description = "json object")
                              String message,
                              @Context SecurityContext ctx) {

        final Principal principal = ctx.getUserPrincipal();
        if (principal == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        if (community_id == null) {
            switch(collection) {
                case OpenEBenchCollection.COMMUNITY:
                case OpenEBenchCollection.PRIVILEGE:
                case OpenEBenchCollection.REFERENCE:
                case OpenEBenchCollection.ID_SOLV: break;
                default:
                    return Response.status(Response.Status.BAD_REQUEST).entity("no 'community_id' parameter").build();
            }
            
        } else if (!OpenEBenchCollection.COMMUNITY.equals(collection) && !community_id.matches("OEBC\\d\\d\\d")) {
            return Response.status(Response.Status.BAD_REQUEST).entity("invalid 'community_id' parameter").build();
        }
        
        try {
            final String _id = URLDecoder.decode(id, "UTF-8");
            final Map<String, Object> result = getDAO().putObject(collection, _id, message, community_id, new OEBSecurityContext(ctx));
            if (result != null) {
                return Response.status(Response.Status.CREATED).entity(result).build();
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(AbstractOEBProtectedService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        } catch (ValidationException ex) {
            final JsonArrayBuilder jb = Json.createArrayBuilder();
            for (ValidationError error : ex.errors) {
                if (error.id == null) {
                    jb.add(Json.createObjectBuilder()
                        .add("error", error.message));
                } else {
                    jb.add(Json.createObjectBuilder()
//                            .add("schema_file", error.id.toString())
                            .add("pointer", error.pointer)
                            .add("path", error.path)
                            .add("error", error.message));
                }
            }
            return Response.status(Response.Status.BAD_REQUEST).entity(jb.build()).build();
        }
        
        return Response.notModified().build();
    }
        
    @PATCH
    @Operation(
        summary = "Patch the object using mongodb 'upsert' operation",
        responses = {
            @ApiResponse(responseCode = "201", description = "object has been put"),
            @ApiResponse(responseCode = "400", description = "validation error")
        }
    )
    @PermitAll
    @Path("/{collection}/{id : .*}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response patchObject(@PathParam("collection") 
                                @Parameter(description = "data collection",
                                           example = "BenchmarkingEvent")
                                String collection, 
                                @PathParam("id")
                                @Parameter(description = "object id",
                                           example = "CAMEO-3D:27")
                                String id,
                                @Parameter(description = "json object")
                                String message,
                                @Context SecurityContext ctx) {

        final Principal principal = ctx.getUserPrincipal();
        if (principal == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        try {
            final String _id = URLDecoder.decode(id, "UTF-8");
            final String result = getDAO().patchObject(collection, _id, message, null, new OEBSecurityContext(ctx));
            if (result == null) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(AbstractOEBProtectedService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        } catch (ValidationException ex) {
            Logger.getLogger(AbstractOEBProtectedService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Operation(
        summary = "Put multiple objects in a bulk mode",
        description = "client sends an array of objects of any type. " +
                      "the type and id are taken from the '_id' and '_schema' attributes. " +
                      "the service respects foreign keys, but sending disordered object " + 
                      "slows down processing speed and memory usage.",
        responses = {
            @ApiResponse(responseCode = "200", description = "all objects inserted"),
            @ApiResponse(responseCode = "400", description = "validation error")
        }
    )
    @PermitAll
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addObjects(@QueryParam("community_id")
                               @Parameter(description = "community id",
                                          example = "OEBC004")
                               String community_id,
                               Reader reader,
                               @Context SecurityContext ctx) {

        final Principal principal = ctx.getUserPrincipal();
        if (principal == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        try (JsonParser parser = Json.createParser(reader)) {
            if (parser.hasNext() &&
                parser.next() == JsonParser.Event.START_ARRAY) {

                final LinkedList<JsonObject> sinker = new LinkedList<>();
                final ArrayList<ValidationException> errors = new ArrayList<>();

                final ByteArrayOutputStream output = new ByteArrayOutputStream();
                final JsonGenerator jgenerator = Json.createGeneratorFactory(
                        Collections.singletonMap(JsonGenerator.PRETTY_PRINTING, true))
                        .createGenerator(new OutputStreamWriter(output, StandardCharsets.UTF_8));
                try {
                    jgenerator.writeStartArray();
                    final Stream<JsonValue> stream = parser.getArrayStream();
                    final Iterator<JsonValue> iterator = stream.iterator();
                    while (iterator.hasNext()) {
                        final JsonValue item = iterator.next();
                        if (JsonValue.ValueType.OBJECT == item.getValueType()) {
                            final JsonObject object = item.asJsonObject();
                            try {
                                final Map<String, Object> obj = 
                                        getDAO().putObject(object, community_id, new OEBSecurityContext(ctx));
                                if (obj != null) {
                                    writeObject(jgenerator, obj);
                                }
                            } catch(ValidationException ex) {
                                boolean fk_only = true;
                                for (ValidationError error : ex.errors) {
                                    if (!(error instanceof FKValidationError)) {
                                        fk_only = false;
                                    }
                                }
                                if (fk_only) {
                                    sinker.add(object);
                                } else {
                                    writeErrors(jgenerator, object, ex);
                                }   
                            }
                        }
                    }
                    if (!sinker.isEmpty()) {
                        int sz;
                        do {
                            sz = sinker.size();
                            errors.clear();

                            for (int i = 0, n = sinker.size(); i < n; i++) {
                                final JsonObject object = sinker.poll();
                                try {
                                    final Map<String, Object> obj = 
                                            getDAO().putObject(object, community_id, new OEBSecurityContext(ctx));
                                    if (obj != null) {
                                        writeObject(jgenerator, obj);
                                    }
                                } catch(ValidationException ex) {
                                    sinker.add(object);
                                    errors.add(ex);
                                }
                            }
                        } while (sinker.size() > 0 && sinker.size() < sz);
                        for (int i = 0, n = sinker.size(); i < n; i++) {
                            writeErrors(jgenerator, sinker.poll(), errors.get(i));
                        }
                    }
                } finally {
                    jgenerator.writeEnd();
                    jgenerator.close();
                }
                StreamingOutput result = (OutputStream out) -> {
                    out.write(output.toByteArray());
                };
                return Response.status(Response.Status.OK).entity(result).build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        } catch(Exception ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }

    @POST
    @Operation(
        summary = "Put multiple objects of the same collection in a bulk mode",
        description = "client sends an array of objects of the same type. " +
                      "the id is taken from the '_id' attribute. ",
        responses = {
            @ApiResponse(responseCode = "200", description = "all objects inserted"),
            @ApiResponse(responseCode = "400", description = "validation error")
        }
    )
    @PermitAll
    @Path("/{collection}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addObjects(@PathParam("collection") 
                               String collection,
                               @QueryParam("community_id")
                               @Parameter(description = "community id",
                                          example = "OEBC004")
                               String community_id,
                               Reader reader,
                               @Context SecurityContext ctx) {

        final Principal principal = ctx.getUserPrincipal();
        if (principal == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        
        if (community_id == null) {
            switch(collection) {
                case OpenEBenchCollection.COMMUNITY:
                case OpenEBenchCollection.PRIVILEGE:
                case OpenEBenchCollection.REFERENCE:
                case OpenEBenchCollection.ID_SOLV: break;
                default:
                    return Response.status(Response.Status.BAD_REQUEST).entity("no 'community_id' parameter").build();
            }
            
        } else if (!OpenEBenchCollection.COMMUNITY.equals(collection) && !community_id.matches("OEBC\\d\\d\\d")) {
            return Response.status(Response.Status.BAD_REQUEST).entity("invalid 'community_id' parameter").build();
        }

        try (JsonParser parser = Json.createParser(reader)) {
            if (parser.hasNext() &&
                parser.next() == JsonParser.Event.START_ARRAY) {
                StreamingOutput result = (OutputStream out) -> {
                    final LinkedList<JsonObject> sinker = new LinkedList<>();
                    final ArrayList<ValidationException> errors = new ArrayList<>();

                    try (JsonGenerator jgenerator = Json.createGenerator(out)) {
                        jgenerator.writeStartArray();
                        
                        final Stream<JsonValue> stream = parser.getArrayStream();
                        final Iterator<JsonValue> iterator = stream.iterator();
                        while(iterator.hasNext()) {
                            final JsonValue item = iterator.next();
                            if (JsonValue.ValueType.OBJECT == item.getValueType()) {
                                final JsonObject object = item.asJsonObject();
                                try {
                                    final Map<String, Object> obj = 
                                            getDAO().putObject(collection, object, community_id, new OEBSecurityContext(ctx));
                                    if (obj != null) {
                                        writeObject(jgenerator, obj);
                                    }
                                } catch(ValidationException ex) {
                                    boolean fk_only = true;
                                    for (ValidationError error : ex.errors) {
                                        if (!(error instanceof FKValidationError)) {
                                            fk_only = false;
                                        }
                                    }
                                    if (fk_only) {
                                        sinker.add(object);
                                    } else {
                                        writeErrors(jgenerator, object, ex);
                                    }
                                }
                            }
                        }
                        if (!sinker.isEmpty()) {
                            int sz;
                            do {
                                sz = sinker.size();
                                errors.clear();

                                for (int i = 0, n = sinker.size(); i < n; i++) {
                                    final JsonObject object = sinker.poll();
                                    try {
                                        final Map<String, Object> obj = 
                                                getDAO().putObject(collection, object, community_id, new OEBSecurityContext(ctx));
                                        if (obj != null) {
                                            writeObject(jgenerator, obj);
                                        }
                                    } catch(ValidationException ex) {
                                        sinker.add(object);
                                        errors.add(ex);
                                    }
                                }
                            } while (sinker.size() > 0 && sinker.size() < sz);
                            for (int i = 0, n = sinker.size(); i < n; i++) {
                                writeErrors(jgenerator, sinker.poll(), errors.get(i));
                            }
                        }
                        jgenerator.writeEnd();
                    } catch (Exception ex) {
                        throw ex;
                    }
                };
                return Response.status(Response.Status.ACCEPTED).entity(result).build();
            } else {
                parser.close();
                return Response.status(Response.Status.BAD_REQUEST).build();
            }                       
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private void writeObject(JsonGenerator jgenerator, Map<String, Object> object) {
        jgenerator.writeStartObject();
        
        final Object _id = object.get("_id");
        jgenerator.write("_id", _id.toString());
        jgenerator.write("_schema", object.get("_schema").toString());

        final Object orig_id = object.get("orig_id");
        if (orig_id != null) {
            jgenerator.write("input_id", orig_id.toString());
        } else {
            jgenerator.write("input_id", OpenEBenchID.getOpenEBenchID(_id));
        }

        jgenerator.writeEnd();
    }
    
    private void writeErrors(JsonGenerator jgenerator, JsonObject object, ValidationException exception) {
        jgenerator.writeStartObject();
        
        final String _schema = object.getString("_schema", null);
        if (_schema != null) {
            jgenerator.write("_schema", _schema);
        }
        
        jgenerator.write("input_id", exception.id);
        
        jgenerator.writeStartArray("errors");
        for (ValidationError error : exception.errors) {
            jgenerator.writeStartObject();
            if (error.pointer != null) {
                jgenerator.write("pointer", error.pointer);
            }
            if (error.path != null) {
                jgenerator.write("path", error.path);
            }
            jgenerator.write("message", error.message);
            jgenerator.writeEnd();
        }
        jgenerator.writeEnd(); // array
        jgenerator.writeEnd();
    }
}
