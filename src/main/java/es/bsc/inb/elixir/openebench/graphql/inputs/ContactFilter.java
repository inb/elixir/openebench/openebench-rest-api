/**
 * *****************************************************************************
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.inputs;

import javax.json.bind.annotation.JsonbProperty;
import org.eclipse.microprofile.graphql.Input;

/**
 * @author Dmitry Repchevsky
 */

@Input("ContactFilters")
public class ContactFilter {
    private String id;
    private String email;
    private String community_id;
    private Link link;

    @JsonbProperty("_id")
    public String getID() {
        return id;
    }

    @JsonbProperty("id")
    public void setId(String id) {
        this.id = id;
    }
    
    @JsonbProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonbProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }
    
    @JsonbProperty("community_id")
    public String getCommunityId() {
        return community_id;
    }

    @JsonbProperty("community_id")
    public void setCommunityId(String community_id) {
        this.community_id = community_id;
    }
    
    @JsonbProperty("links")
    public Link getLink() {
        return link;
    }
    
    @JsonbProperty("links")
    public void setLink(Link link) {
        this.link = link;
    }

    public static class Link {
        private String label;
        private String uri;
        
        @JsonbProperty("label")
        public String getLabel() {
            return label;
        }

        @JsonbProperty("label")
        public void setLabel(String label) {
            this.label = label;
        }

        @JsonbProperty("uri")
        public String getURI() {
            return uri;
        }

        @JsonbProperty("uri")
        public void setURI(String uri) {
            this.uri = uri;
        }
    }
}
