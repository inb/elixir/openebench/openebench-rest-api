/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.model;

import java.net.URI;
import java.util.List;
import javax.json.bind.annotation.JsonbProperty;

/**
 * @author Dmitry Repchevsky
 */

public class ToolAccess {
    private String tool_access_type;
    private URI link;
    private List<Link> documentation;

    public ToolAccess() {}

    @JsonbProperty("tool_access_type")
    public String getToolAccessType() {
        return tool_access_type;
    }
    
    @JsonbProperty("tool_access_type")
    public void setToolAccessType(String tool_access_type) {
        this.tool_access_type = tool_access_type;
    }
    
    @JsonbProperty("link")
    public URI getLink() {
        return link;
    }
    
    @JsonbProperty("link")
    public void setLink(URI link) {
        this.link = link;
    }
    
    @JsonbProperty("techdocs")
    public List<Link> getTechnicalDocumentation() {
        return documentation;
    }
    
    @JsonbProperty("techdocs")
    public void setTechnicalDocumentation(List<Link> documentation) {
        this.documentation = documentation;
    }
}
