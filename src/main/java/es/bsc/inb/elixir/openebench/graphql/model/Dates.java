/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.model;

import java.time.ZonedDateTime;
import javax.json.bind.annotation.JsonbProperty;

/**
 * @author Dmitry Repchevsky
 */

public class Dates {
    private ZonedDateTime creation_date;
    private ZonedDateTime modification_date;
    private ZonedDateTime public_date;

    public Dates() {}

    @JsonbProperty("creation")
    public ZonedDateTime getCreationDate() {
        return creation_date;
    }

    @JsonbProperty("creation")
    public void setCreationDate(ZonedDateTime creation_date) {
        this.creation_date = creation_date;
    }

    @JsonbProperty("modification")
    public ZonedDateTime getModificationDate() {
        return modification_date;
    }

    @JsonbProperty("modification")
    public void setModificationDate(ZonedDateTime modification_date) {
        this.modification_date = modification_date;
    }

    @JsonbProperty("public")
    public ZonedDateTime getPublicDate() {
        return public_date;
    }

    @JsonbProperty("public")
    public void setPublicDate(ZonedDateTime public_date) {
        this.public_date = public_date;
    }
}
