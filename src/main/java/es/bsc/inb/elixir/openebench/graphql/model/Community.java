/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.model;

import java.time.ZonedDateTime;
import java.util.List;
import javax.json.JsonObject;
import javax.json.bind.annotation.JsonbProperty;
import org.eclipse.microprofile.graphql.Type;

/**
 * @author Dmitry Repchevsky
 */

@Type("Community")
public class Community {
    private String id;
    private String schema;
    private String name;
    private String acronym;
    private String status;
    private String description;
    private JsonObject metadata;
    private List<String> keywords;
    private List<CommunityLink> links;
    private List<String> reference_ids;
    private List<String> community_contact_ids;
    private List <ReferenceTool> reference_tools;

    private ZonedDateTime created;
    private ZonedDateTime updated;

    private List<BenchmarkingEvent> benchmarking_events;
    
    public Community() {}

    @JsonbProperty("_id")
    public String getID() {
        return id;
    }
    
    @JsonbProperty("_id")
    public void setID(String id) {
        this.id = id;
    }
    
    @JsonbProperty("_schema")
    public String getSchema() {
        return schema;
    }
    
    @JsonbProperty("_schema")
    public void setSchema(String schema) {
        this.schema = schema;
    }

    @JsonbProperty("name")
    public String getName() {
        return name;
    }
    
    @JsonbProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonbProperty("acronym")
    public String getAcronym() {
        return acronym;
    }
    
    @JsonbProperty("acronym")
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    @JsonbProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonbProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonbProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonbProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonbProperty("_metadata")
    public JsonObject getMetadata() {
        return metadata;
    }

    @JsonbProperty("_metadata")
    public void setMetadata(JsonObject metadata) {
        this.metadata = metadata;
    }

    @JsonbProperty("keywords")
    public List<String> getKeywords() {
        return keywords;
    }

    @JsonbProperty("keywords")
    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    @JsonbProperty("links")
    public List<CommunityLink> getLinks() {
        return links;
    }

    @JsonbProperty("links")
    public void setLinks(List<CommunityLink> links) {
        this.links = links;
    }

    @JsonbProperty("references")
    public List<String> getReferenceIDs() {
        return reference_ids;
    }

    @JsonbProperty("references")
    public void setReferenceIDs(List<String> reference_ids) {
        this.reference_ids = reference_ids;
    }

    @JsonbProperty("community_contact_ids")
    public List<String> getCommunityContactIDs() {
        return community_contact_ids;
    }

    @JsonbProperty("community_contact_ids")
    public void setCommunityContactIDs(List<String> community_contact_ids) {
        this.community_contact_ids = community_contact_ids;
    }    

    @JsonbProperty("reference_tools")
    public List<ReferenceTool> getReferenceTools() {
        return reference_tools;
    }

    @JsonbProperty("reference_tools")
    public void setReferenceTools(List<ReferenceTool> reference_tools) {
        this.reference_tools = reference_tools;
    }

    @JsonbProperty("_created")
    public ZonedDateTime getCreated() {
        return created;
    }

    @JsonbProperty("_created")
    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    @JsonbProperty("_updated")
    public ZonedDateTime getUpdated() {
        return updated;
    }

    @JsonbProperty("_updated")
    public void setUpdated(ZonedDateTime updated) {
        this.updated = updated;
    }

    @JsonbProperty("benchmarkingEvents")
    public List<BenchmarkingEvent> getBenchmarkingEvents() {
        return benchmarking_events;
    }

    @JsonbProperty("benchmarkingEvents")
    public void setBenchmarkingEvents(List<BenchmarkingEvent> benchmarking_events) {
        this.benchmarking_events = benchmarking_events;
    }
}
