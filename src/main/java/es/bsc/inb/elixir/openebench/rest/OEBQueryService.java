/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */
package es.bsc.inb.elixir.openebench.rest;

import es.bsc.inb.elixir.openebench.auth.Authorization;
import es.bsc.inb.elixir.openebench.mongo.MongoConnection;
import es.bsc.inb.elixir.openebench.mongo.ProductionQuery;
import es.bsc.inb.elixir.openebench.security.OEBSecurityContext;
import io.swagger.v3.oas.annotations.Parameter;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.security.PermitAll;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.Encoded;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * @author Dmitry Repchevsky
 */

@Path("/query")
@ApplicationScoped
public class OEBQueryService {
    
    @Inject
    private MongoConnection mongo;

    private ProductionQuery production;
    
    @PostConstruct
    public void init() {
        production = new ProductionQuery(new Authorization(mongo.getStagedDatabase(), null));
    }

    @GET
    @Path("/contacts/{id : OEBC[0-9]{3}}")
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContactsForCommunity(@PathParam("id")
                                            @Parameter(description = "id of the community",
                                                       example = "OEBC003")
                                            @Encoded String community_id,
                                            @Context SecurityContext ctx) {

        final JsonObject object = production.getContactsForCommunity(community_id, new OEBSecurityContext(ctx));
        if (object == null) {
            return Response.serverError().build();
        }
        if (object.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(object).build();
    }

    @GET
    @Path("/contacts/{id : OEBE[0-9]{3}[A-Z0-9]{7}}")
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContactsByBenchmarkingEvent(
            @PathParam("id")
            @Parameter(description = "id of the benchmarking event",
            example = "")
            @Encoded String benchmarking_event_id,
            @Context SecurityContext ctx) {

        final JsonObject object = production.getContactsForBenchmarkingEvent(benchmarking_event_id, new OEBSecurityContext(ctx));
        if (object == null) {
            return Response.serverError().build();
        }
        if (object.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(object).build();
    }

    @GET
    @Path("/contacts/{id : OEBX[0-9]{3}[A-Z0-9]{7}}")
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContactsByChallenge(@PathParam("id")
                                           @Parameter(description = "id of the challenge",
                                                      example = "")
                                           @Encoded String challenge_id,
                                           @Context SecurityContext ctx) {

        final JsonObject object = production.getContactsForChallenge(challenge_id, new OEBSecurityContext(ctx));
        if (object == null) {
            return Response.serverError().build();
        }
        if (object.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(object).build();
    }
    
    @GET
    @Path("/tools/{id : OEBE[0-9]{3}[A-Z0-9]{7}}")
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public Response getToolsByEventId(@PathParam("id")
                                      @Parameter(description = "id of the event",
                                                 example = "")
                                      @Encoded String event_id,
                                      @QueryParam("projection")
                                      List<String> projections,
                                      @Context SecurityContext ctx) {

        final Collection<JsonObject> tools = production.getToolsForBenchmarkingEvent(event_id, projections, new OEBSecurityContext(ctx));
        return Response.ok(tools, MediaType.WILDCARD_TYPE).build();
    }
    
    @GET
    @Path("/privileges/")
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPrivileges(@Context SecurityContext ctx) {

        final JsonObject privileges = production.getPrivileges(new OEBSecurityContext(ctx));
        return Response.ok(privileges, MediaType.WILDCARD_TYPE).build();
    }
    
}
