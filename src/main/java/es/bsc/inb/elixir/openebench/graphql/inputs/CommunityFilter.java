/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.inputs;

import java.util.List;
import javax.json.bind.annotation.JsonbProperty;
import org.eclipse.microprofile.graphql.Input;

/**
 * @author Dmitry Repchevsky
 */

@Input("CommunityFilters")
public class CommunityFilter {
    private String id;
    private List<String> keywords;
    private String status;
    private BenchmarkingEventFilter benchmarking_event;

    public CommunityFilter() {}
    
    @JsonbProperty("_id")
    public String getID() {
        return id;
    }

    @JsonbProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonbProperty("filters")
    public BenchmarkingEventFilter getBenchmarkingEvent() {
        return benchmarking_event;
    }

    @JsonbProperty("filters")
    public void setBenchmarkingEventFilters(BenchmarkingEventFilter benchmarking_event) {
        this.benchmarking_event = benchmarking_event;
    }
}
