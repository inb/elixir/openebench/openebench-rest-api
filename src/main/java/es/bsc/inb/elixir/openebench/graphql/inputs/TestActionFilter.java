/**
 * *****************************************************************************
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.inputs;

import es.bsc.inb.elixir.openebench.graphql.CompositeID;
import javax.json.bind.annotation.JsonbProperty;

/**
 * @author Dmitry Repchevsky
 */

public class TestActionFilter {
    
    private String id;
    private String tool_id;
    private String action_type;
    private String challenge_id;
    
    @JsonbProperty("_id")
    public CompositeID getID() {
        return id == null ? null : new CompositeID(id);
    }

    @JsonbProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonbProperty("tool_id")
    public String getToolID() {
        return tool_id;
    }
    
    @JsonbProperty("tool_id")
    public void setToolID(String tool_id) {
        this.tool_id = tool_id;
    }

    @JsonbProperty("action_type")
    public String getActionType() {
        return action_type;
    }
    
    @JsonbProperty("action_type")
    public void setActionType(String action_type) {
        this.action_type = action_type;
    }
    
    @JsonbProperty("challenge_id")
    public String getChallengeID() {
        return challenge_id;
    }
    
    @JsonbProperty("challenge_id")
    public void setChallengeID(String challenge_id) {
        this.challenge_id = challenge_id;
    }

}
