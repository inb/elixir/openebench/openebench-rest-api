/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.model;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.List;
import javax.json.JsonObject;
import javax.json.bind.annotation.JsonbProperty;
import org.eclipse.microprofile.graphql.Type;

/**
 * @author Dmitry Repchevsky
 */

@Type("Datalink")
public class Datalink {

    private JsonObject inline_data;
    private URI schema_url;

    private URI uri;
    private List<String> attributes;
    private ZonedDateTime validation_date;
    private String status;

    @JsonbProperty("inline_data")
    public JsonObject getInlineData() {
        return inline_data;
    }

    @JsonbProperty("inline_data")
    public void setInlineData(JsonObject inline_data) {
        this.inline_data = inline_data;
    }    

    @JsonbProperty("schema_url")
    public URI getSchemaURL() {
        return schema_url;
    }

    @JsonbProperty("schema_url")
    public void setSchemaURL(URI schema_url) {
        this.schema_url = schema_url;
    }

    @JsonbProperty("uri")
    public URI getURI() {
        return uri;
    }

    @JsonbProperty("uri")
    public void setURI(URI uri) {
        this.uri = uri;
    }

    @JsonbProperty("attrs")
    public List<String> getAttributes() {
        return attributes;
    }

    @JsonbProperty("attrs")
    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    @JsonbProperty("validation_date")
    public ZonedDateTime getValidationDate() {
        return validation_date;
    }

    @JsonbProperty("validation_date")
    public void setValidationDate(ZonedDateTime validation_date) {
        this.validation_date = validation_date;
    }

    @JsonbProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonbProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }
}
