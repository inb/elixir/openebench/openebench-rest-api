/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.rest;

import es.bsc.inb.elixir.openebench.mongo.OpenEBenchDAO;
import es.bsc.inb.elixir.openebench.security.OEBSecurityContext;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * @author Dmitry Repchevsky
 */

@Path("/staged")
@ApplicationScoped
public class OEBStagedService extends AbstractOEBProtectedService {
    
    @Inject
    @Named("staged")
    private OpenEBenchDAO staged;
    
    @Override
    public OpenEBenchDAO getDAO() {
        return staged;
    }

    @DELETE
    @PermitAll
    @Path("/{collection}/{id : .*}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteObject(@PathParam("collection") String collection, 
                                 @PathParam("id") String id,
                                 @Context SecurityContext ctx) {
        
        final Principal principal = ctx.getUserPrincipal();
        if (principal == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        
        try {
            final String _id = URLDecoder.decode(id, "UTF-8");

            final StringWriter writer = new StringWriter();
            final Boolean result = getDAO().removeObject(writer, _id, collection, new OEBSecurityContext(ctx));
            if (result == null) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(writer.toString()).build(); 
            }
            return result ? Response.ok().build() : Response.status(Response.Status.NOT_FOUND).entity(writer.toString()).build();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(AbstractOEBProtectedService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
