/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.rest;

import es.bsc.inb.elixir.openebench.mongo.OpenEBenchDAO;
import es.bsc.inb.elixir.openebench.security.OEBSecurityContext;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.StreamingOutput;

/**
 * @author Dmitry Repchevsky
 */

@Path("/sandbox")
@ApplicationScoped
public class OEBSandboxService extends AbstractOEBProtectedService {

    @Inject
    @Named("sandbox")
    private OpenEBenchDAO sandbox;
    
    @Override
    public OpenEBenchDAO getDAO() {
        return sandbox;
    }

    @GET
    @PermitAll
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getObjects(@Context SecurityContext ctx) {

        StreamingOutput stream = (OutputStream out) -> {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"))) {
                getDAO().writeObjects(writer, false, new OEBSecurityContext(ctx));
            } catch(Exception ex) {
                Logger.getLogger(AbstractOEBProtectedService.class.getName()).log(Level.SEVERE, null, ex);
            }
        };
        return Response.ok(stream, MediaType.APPLICATION_JSON).build();
    }
    
    @DELETE
    @PermitAll
    @Path("/")
    public Response removeObjects(@Context SecurityContext ctx) {
        getDAO().drop(new OEBSecurityContext(ctx));
        return Response.ok().build();
    }
}
