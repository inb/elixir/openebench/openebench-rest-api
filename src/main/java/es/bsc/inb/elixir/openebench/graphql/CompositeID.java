/**
 * *****************************************************************************
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql;

/**
 * @author Dmitry Repchevsky
 */

public class CompositeID {
    
    private String id;
    private Integer revision;
    
    public CompositeID() {}
    
    public CompositeID(final String id) {
        final int idx = id.lastIndexOf('.');
        if (idx < 0) {
            this.id = id;
        } else {
            final String suffix = id.substring(idx + 1);
            try {
                this.revision = Integer.parseUnsignedInt(suffix);
                this.id = id.substring(0, idx);
            } catch (NumberFormatException ex) {
                this.id = id;
            }
        }
    }
    
    public CompositeID(final String id, final Integer revision) {
        this.id = id;
        this.revision = revision;
    }
    
    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }
    
    public Integer getRevision() {
        return revision;
    }

    public void setRevision(final Integer revision) {
        this.revision = revision;
    }

    @Override
    public String toString() {
        return revision == null ? id : id + "." + revision; 
    }
}
