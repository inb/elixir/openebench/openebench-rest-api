/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.application;

import es.bsc.inb.elixir.openebench.mongo.MongoConnection;
import es.bsc.inb.elixir.openebench.validator.AbstractOEBSchemaResolver;
import es.bsc.inb.elixir.openebench.validator.OpenEBenchSchemaResolver;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.ServletContext;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class OEBSchemaLocatorConfig {

    @Inject 
    private ServletContext ctx;
    
    private AbstractOEBSchemaResolver resolver;

    public AbstractOEBSchemaResolver getSchemaResolver() {
        return resolver;
    }

    @PostConstruct
    public void init() {
        final String schemas_location_uri = ctx.getInitParameter("json.schemas.uri");
        if (schemas_location_uri == null || schemas_location_uri.isEmpty()) {
            Logger.getLogger(MongoConnection.class.getName()).log(Level.SEVERE, "no 'json.schemas.uri' json schema location defined");
            return;
        }
        
        resolver = new OpenEBenchSchemaResolver(schemas_location_uri);
    }
}
