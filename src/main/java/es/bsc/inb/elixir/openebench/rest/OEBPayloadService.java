/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.rest;

import es.bsc.inb.elixir.openebench.application.OEBSchemaLocatorConfig;
import es.bsc.inb.elixir.openebench.auth.Authorization;
import es.bsc.inb.elixir.openebench.mongo.MongoConnection;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchDAO;
import es.bsc.inb.elixir.openebench.security.OEBSecurityContext;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;

/**
 * @author Dmitry Repchevsky
 */

@Path("/payload")
@ApplicationScoped
public class OEBPayloadService {
    
    @Resource
    private ManagedExecutorService executor;

    @Inject
    private MongoConnection mongo;
    
    @Inject
    private OEBSchemaLocatorConfig locator;
    
    private OpenEBenchDAO dao;
    
    @PostConstruct
    public void init() {
        dao = new OpenEBenchDAO(
                new Authorization(mongo.getStagedDatabase(), null),
                locator.getSchemaResolver());
    }

    @Operation(
        summary = "Check whether the payload file exists",
        responses = {
            @ApiResponse(responseCode = "200", description = "file exists"),
            @ApiResponse(responseCode = "400", description = "invalid id"),
            @ApiResponse(responseCode = "404", description = "file not found")
        }
    )
    @HEAD
    @PermitAll
    @Path("/{id : .*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response hasFile(@Context UriInfo uriInfo,
                            @Context SecurityContext ctx) {

        final String base = uriInfo.getBaseUriBuilder().path(OEBPayloadService.class).build().toString();
        final String id = uriInfo.getAbsolutePath().toString().substring(base.length() + 1);
        
        final String exist = dao.hasPayload(id, true, new OEBSecurityContext(ctx));
        return exist != null ?
                Response.ok(MediaType.APPLICATION_JSON).build() : 
                Response.status(Response.Status.NOT_FOUND).build();
    }
    
    @Operation(
        summary = "dowload the payload file",
        responses = {
            @ApiResponse(responseCode = "200", description = "file was uploaded"),
            @ApiResponse(responseCode = "400", description = "error")
        }
    )
    @GET
    @PermitAll
    @Path("/{id : .*}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getFile(@Context UriInfo uriInfo,
                            @Context HttpHeaders httpHeaders,
                            @Context SecurityContext ctx) {

        final String base = uriInfo.getBaseUriBuilder().path(OEBPayloadService.class).build().toString();
        final String id = uriInfo.getAbsolutePath().toString().substring(base.length() + 1);
        
        final Map<String, String> headers = new HashMap();
        final String since = httpHeaders.getHeaderString(HttpHeaders.IF_MODIFIED_SINCE);
        if (since != null) {
            headers.put(HttpHeaders.IF_MODIFIED_SINCE, since);
        }
        
        final InputStream in = dao.getPayloadStream(headers, id, true, new OEBSecurityContext(ctx));
        final String last_modified = headers.get(HttpHeaders.LAST_MODIFIED);
        
        if (in == null) {
            return last_modified == null ? Response.status(Status.NOT_FOUND).build()
                    : Response.status(Status.NOT_MODIFIED).header(HttpHeaders.LAST_MODIFIED, last_modified).build();
        }

        final StreamingOutput stream = (OutputStream out) -> {
            in.transferTo(out);
        };

        return Response.ok(stream)
                .header(HttpHeaders.LAST_MODIFIED, last_modified)
                .header("X-OEB-METADATA", 
                headers.getOrDefault("X-OEB-METADATA", "")).build();
    }

    @Operation(
        summary = "upload the payload file into the database",
        responses = {
            @ApiResponse(responseCode = "201", description = "file was uploaded"),
            @ApiResponse(responseCode = "400", description = "error")
        }
    )
    @PUT
    @PermitAll
    @Path("/{id : .*}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_OCTET_STREAM)
    public Response uploadFile(InputStream in,
                               @HeaderParam("X-OEB-METADATA") String metadata,
                               @Context UriInfo uriInfo,
                               @Context SecurityContext ctx) {

        final Principal principal = ctx.getUserPrincipal();
        if (principal == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        final String base = uriInfo.getBaseUriBuilder().path(OEBPayloadService.class).build().toString();
        final String id = uriInfo.getAbsolutePath().toString().substring(base.length() + 1);
        
        final String meta = dao.putPayload(id, in, metadata, true, new OEBSecurityContext(ctx));
        if (meta == null) {
            return Response.status(Status.UNAUTHORIZED).build();
        }
        return Response.ok(meta).build();
    }
}
