/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.rest;

import es.bsc.inb.elixir.openebench.mongo.OpenEBenchCollection;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchDAO;
import es.bsc.inb.elixir.openebench.security.OEBSecurityContext;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Encoded;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;

/**
 * OpenEBench 'GET' implementation 
 * (shared between 'public', 'staged' and 'sandbox' endpoints).
 * 
 * @author Dmitry Repchevsky
 */

public abstract class AbstractOEBPublicService {

    @Context protected UriInfo uri_info;
    @Context protected HttpServletResponse response;
        
    /**
     * Method may by overwritten to explicitly forbid the access to some collections.
     * 
     * @param collection the name of collection to be checked
     * @return false
     */
    public boolean isForbidden(String collection) {
        return false;
    }

    abstract OpenEBenchDAO getDAO();
    
    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @HttpMethod("PATCH")
    public @interface PATCH {}
    
    @Hidden
    @PermitAll
    @Path("/{path: .*}")
    @OPTIONS
    public Response compliance() {
        return Response.ok().header("Allow", "GET")
                            .header("Allow", "HEAD")
                            .header("Allow", "PUT")
                            .header("Allow", "POST")
                            .header("Allow", "PATCH")
                            .header("Allow", "DELETE")
                            .build();
    }

    @GET
    @Operation(summary = "Lists the collection data identifiers",
        responses = {
            @ApiResponse(content = @Content(
               mediaType = "text/uri-list"
            ))
        }
    )
    @Path("/{collection}")
    @Produces("text/uri-list")
    public Response getIds(@PathParam("collection") 
                           @Parameter(description = "data collection",
                                      example = "BenchmarkingEvent")
                           String collection) {

        StreamingOutput stream = (OutputStream out) -> {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"))) {
                getDAO().writeIDs(writer, collection);
            } catch(Exception ex) {
                Logger.getLogger(AbstractOEBProtectedService.class.getName()).log(Level.SEVERE, null, ex);
            }
        };
        return Response.ok(stream).build();
    }

    @GET
    @PermitAll
    @Path("/{collection}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getObjects(@PathParam("collection")
                               @Parameter(description = "OpenEBench collection",
                                          example = "Contact")
                               @Encoded String collection,
                               @QueryParam("privileged")
                               @Parameter(description = "list only objects user has privileges for",
                                          example = "true")
                               String privileged,
                               @Context SecurityContext ctx) {

        StreamingOutput stream = (OutputStream out) -> {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"))) {
                getDAO().writeObjects(writer, collection, Boolean.parseBoolean(privileged), new OEBSecurityContext(ctx));
            } catch(Exception ex) {
                Logger.getLogger(AbstractOEBProtectedService.class.getName()).log(Level.SEVERE, null, ex);
            }
        };
        return Response.ok(stream, MediaType.APPLICATION_JSON).build();

    }

    @Operation(
        summary = "Check whether the object exists",
        responses = {
            @ApiResponse(responseCode = "200", description = "object exists"),
            @ApiResponse(responseCode = "400", description = "invalid id"),
            @ApiResponse(responseCode = "404", description = "object not found")
        }
    )
    @HEAD
    @PermitAll
    @Path("/{collection}/{id : .*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response hasObject(@PathParam("collection")
                              @Parameter(description = "data collection",
                                         example = "BenchmarkingEvent")
                              String collection,
                              @PathParam("id")
                              @Parameter(description = "object id",
                                         example = "OEBD00200001FO")
                              @Encoded String id) {
        
        try {
            final String _id = URLDecoder.decode(id, "UTF-8");
            final Boolean exist = getDAO().hasObject(_id, collection);
            return exist != null && exist ? 
                    Response.ok(MediaType.APPLICATION_JSON).build() : 
                    Response.status(Response.Status.NOT_FOUND).build();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(AbstractOEBProtectedService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @PermitAll
    @Path("/Reference/{id : .*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getObject(@PathParam("id")
                              @Parameter(description = "reference id",
                                         example = "doi.org:10.7490/f1000research.1116968.1")
                              @Encoded String id,
                              @Context SecurityContext ctx) {

        final StringBuilder result = new StringBuilder();
        final int status = getDAO().writeObject(result, id, OpenEBenchCollection.REFERENCE, false, new OEBSecurityContext(ctx));
        return Response.status(status).entity(result).type(MediaType.APPLICATION_JSON).build();
    }

    @GET
    @PermitAll
    @Path("/{collection}/{id:.[^/]*}{path : .*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getObject(@PathParam("collection")
                              @Parameter(description = "OpenEBench collection",
                                         example = "Contact")
                              String collection,
                              @PathParam("id")
                              @Parameter(description = "object id",
                                         example = "OEBD00200001FO")
                              @Encoded String id,
                              @PathParam("path")
                              @Parameter(description = "JSON path ",
                                         example = "")
                              String path,
                              @Context SecurityContext ctx) {

        final StringBuilder result = new StringBuilder();
        final int status = getDAO().writeObject(result, id, collection, path, false, new OEBSecurityContext(ctx));
        return Response.status(status).entity(result.toString()).type(MediaType.APPLICATION_JSON).build();
    }
    
    @GET
    @Operation(summary = "Get a Json Schema location for the collection",
        responses = {
            @ApiResponse(content = @Content(
               mediaType = "text/x-uri"
            ))
        }
    )
    @PermitAll
    @Path("/{collection}")
    @Produces("text/x-uri")
    public Response getJsonSchemaURI(@PathParam("collection") 
                                     @Parameter(description = "data collection",
                                                example = "BenchmarkingEvent")
                                     String collection) {

        final String uri = getDAO().getJsonSchemaURI(collection);

        return Response.ok(uri).build();
    }

    @GET
    @Operation(summary = "Get a Json Schema for the collection",
        responses = {
            @ApiResponse(content = @Content(
               mediaType = "application/schema+json"
            ))
        }
    )
    @PermitAll
    @Path("/{collection}")
    @Produces("application/schema+json")
    public Response getJsonSchema(@PathParam("collection") 
                                  @Parameter(description = "data collection",
                                             example = "BenchmarkingEvent")
                                  String collection) {

        final JsonObject schema = getDAO().getJsonSchema(collection);

        return Response.ok(schema).build();
    }

}
