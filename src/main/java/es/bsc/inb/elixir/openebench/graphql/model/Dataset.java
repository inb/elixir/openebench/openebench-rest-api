/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.model;

import java.time.ZonedDateTime;
import java.util.List;
import javax.json.JsonObject;
import javax.json.bind.annotation.JsonbProperty;
import org.eclipse.microprofile.graphql.Type;

/**
 * @author Dmitry Repchevsky
 */

@Type("Dataset")
public class Dataset {
    private String id;
    private Integer revision;
    private String schema;
    private String original_id;
    private List<String> community_ids;
    private List<String> challenge_ids;
    private String visibility;
    private String name;
    private String version;
    private String description;
    private JsonObject metadata;
    private Dates dates;
    private String type;

    private ZonedDateTime created;
    private ZonedDateTime updated;

    private Datalink datalink;
    private List<String> dataset_contact_ids;
    private DatasetTool depends_on;
    private List<String> references;

    public Dataset() {}

    @JsonbProperty("_id")
    public String getID() {
        return id;
    }
    
    @JsonbProperty("_id")
    public void setID(String id) {
        this.id = id;
    }

    @JsonbProperty("_revision")
    public Integer getRevision() {
        return revision;
    }
    
    @JsonbProperty("_revision")
    public void setRevision(Integer revision) {
        this.revision = revision;
    }
    
    @JsonbProperty("_schema")
    public String getSchema() {
        return schema;
    }
    
    @JsonbProperty("_schema")
    public void setSchema(String schema) {
        this.schema = schema;
    }    

    @JsonbProperty("orig_id")
    public String getOriginalID() {
        return original_id;
    }
    
    @JsonbProperty("orig_id")
    public void setOriginalID(String original_id) {
        this.original_id = original_id;
    }

    @JsonbProperty("community_ids")
    public List<String> getCommunityIDs() {
        return community_ids;
    }

    @JsonbProperty("community_ids")
    public void setCommunityIDs(List<String> community_ids) {
        this.community_ids = community_ids;
    }

    @JsonbProperty("challenge_ids")
    public List<String> getChallengeIDs() {
        return challenge_ids;
    }

    @JsonbProperty("challenge_ids")
    public void setChallengeIDs(List<String> challenge_ids) {
        this.challenge_ids = challenge_ids;
    }

    @JsonbProperty("visibility")
    public String getVisibility() {
        return visibility;
    }

    @JsonbProperty("visibility")
    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    @JsonbProperty("name")
    public String getName() {
        return name;
    }

    @JsonbProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonbProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonbProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonbProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonbProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonbProperty("_metadata")
    public JsonObject getMetadata() {
        return metadata;
    }

    @JsonbProperty("_metadata")
    public void setMetadata(JsonObject metadata) {
        this.metadata = metadata;
    }

    @JsonbProperty("dates")
    public Dates getDates() {
        return dates;
    }

    @JsonbProperty("dates")
    public void setDates(Dates dates) {
        this.dates = dates;
    }

    @JsonbProperty("type")
    public String getType() {
        return type;
    }

    @JsonbProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonbProperty("datalink")
    public Datalink getDatalink() {
        return datalink;
    }

    @JsonbProperty("_created")
    public ZonedDateTime getCreated() {
        return created;
    }

    @JsonbProperty("_created")
    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    @JsonbProperty("_updated")
    public ZonedDateTime getUpdated() {
        return updated;
    }

    @JsonbProperty("_updated")
    public void setUpdated(ZonedDateTime updated) {
        this.updated = updated;
    }

    @JsonbProperty("datalink")
    public void setDatalink(Datalink datalink) {
        this.datalink = datalink;
    }

    @JsonbProperty("dataset_contact_ids")
    public List<String> getDatasetContactIDs() {
        return dataset_contact_ids;
    }

    @JsonbProperty("dataset_contact_ids")
    public void setDatasetContactIDs(List<String> dataset_contact_ids) {
        this.dataset_contact_ids = dataset_contact_ids;
    }

    @JsonbProperty("depends_on")
    public DatasetTool getDependsOn() {
        return depends_on;
    }

    @JsonbProperty("depends_on")
    public void setDependsOn(DatasetTool depends_on) {
        this.depends_on = depends_on;
    }

    @JsonbProperty("references")
    public List<String> getReferences() {
        return references;
    }

    @JsonbProperty("references")
    public void setReferences(List<String> references) {
        this.references = references;
    }
}
