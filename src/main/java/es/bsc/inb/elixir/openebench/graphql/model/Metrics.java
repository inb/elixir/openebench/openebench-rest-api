/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.model;

import java.time.ZonedDateTime;
import java.util.List;
import javax.json.JsonObject;
import javax.json.bind.annotation.JsonbProperty;
import org.eclipse.microprofile.graphql.Type;

/**
 * @author Dmitry Repchevsky
 */

@Type("Metrics")
public class Metrics {
    private String id;
    private Integer revision;
    private String schema;
    private String original_id;
    private String title;
    private String description;
    private JsonObject metadata;
    private List<String> metrics_contact_ids;
    private String formal_definition;
    private String execution_type;
    private String data_schemas;
    private List<Link> links;    
    private List<String> references;
    private JsonObject representation_hints;
    
    private ZonedDateTime created;
    private ZonedDateTime updated;
    
    public Metrics() {}

    @JsonbProperty("_id")
    public String getID() {
        return id;
    }
    
    @JsonbProperty("_id")
    public void setID(String id) {
        this.id = id;
    }

    @JsonbProperty("_revision")
    public Integer getRevision() {
        return revision;
    }
    
    @JsonbProperty("_revision")
    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    @JsonbProperty("_schema")
    public String getSchema() {
        return schema;
    }
    
    @JsonbProperty("_schema")
    public void setSchema(String schema) {
        this.schema = schema;
    }  
    
    @JsonbProperty("orig_id")
    public String getOriginalID() {
        return original_id;
    }
    
    @JsonbProperty("orig_id")
    public void setOriginalID(String original_id) {
        this.original_id = original_id;
    }

    @JsonbProperty("title")
    public String getTitle() {
        return title;
    }
    
    @JsonbProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonbProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonbProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonbProperty("_metadata")
    public JsonObject getMetadata() {
        return metadata;
    }

    @JsonbProperty("_metadata")
    public void setMetadata(JsonObject metadata) {
        this.metadata = metadata;
    }

    @JsonbProperty("metrics_contact_ids")
    public List<String> getMetricsContactIDs() {
        return metrics_contact_ids;
    }
    
    @JsonbProperty("metrics_contact_ids")
    public void setMetricsContactIDs(List<String> metrics_contact_ids) {
        this.metrics_contact_ids = metrics_contact_ids;
    }

    @JsonbProperty("formal_definition")
    public String getFormalDefinition() {
        return formal_definition;
    }

    @JsonbProperty("formal_definition")
    public void setFormalDefinition(String formal_definition) {
        this.formal_definition = formal_definition;
    }
    
    @JsonbProperty("execution_type")
    public String getExecutionType() {
        return execution_type;
    }

    @JsonbProperty("execution_type")
    public void setExecutionType(String execution_type) {
        this.execution_type = execution_type;
    }
    
    @JsonbProperty("data_schemas")
    public String getDataSchemas() {
        return data_schemas;
    }

    @JsonbProperty("data_schemas")
    public void setDataSchemas(String data_schemas) {
        this.data_schemas = data_schemas;
    }
    
    @JsonbProperty("links")
    public List<Link> getMetricsLinks() {
        return links;
    }
    
    @JsonbProperty("links")
    public void setMetricsLinks(List<Link> links) {
        this.links = links;
    }

    @JsonbProperty("references")
    public List<String> getReferences() {
        return references;
    }
    
    @JsonbProperty("references")
    public void setReferences(List<String> references) {
        this.references = references;
    }

    @JsonbProperty("representation_hints")
    public JsonObject getRepresentationHints() {
        return representation_hints;
    }

    @JsonbProperty("representation_hints")
    public void setRepresentationHints(JsonObject representation_hints) {
        this.representation_hints = representation_hints;
    }

    @JsonbProperty("_created")
    public ZonedDateTime getCreated() {
        return created;
    }

    @JsonbProperty("_created")
    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    @JsonbProperty("_updated")
    public ZonedDateTime getUpdated() {
        return updated;
    }

    @JsonbProperty("_updated")
    public void setUpdated(ZonedDateTime updated) {
        this.updated = updated;
    }
}
