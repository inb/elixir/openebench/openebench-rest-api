/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.application;

import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.ws.rs.core.HttpHeaders;

/**
 * Wrapper that injects HTTP BASIC 'anonymous:anonymous' authorization when there is no any.
 * 
 * @author Dmitry Repchevsky
 */

public class AuthorizationHeaderRequestWrapper extends HttpServletRequestWrapper {

    public final static String ANONYMOUSE_USER_CREDENTIALS = "Basic YW5vbnltb3VzOmFub255bW91cw==";

    public AuthorizationHeaderRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getAuthType() {
        return super.getAuthType();
    }
    
    @Override
    public String getHeader(String name) {
        final String header = super.getHeader(name);
        if (HttpHeaders.AUTHORIZATION.equals(name) && header == null) {
            return ANONYMOUSE_USER_CREDENTIALS;
        }
        return header;
    }

    @Override
    public Enumeration<String> getHeaders(String name) {
        if (HttpHeaders.AUTHORIZATION.equals(name) && super.getHeader(HttpHeaders.AUTHORIZATION) == null) {
            List<String> auths = Collections.list(super.getHeaders(name));
            auths.add(ANONYMOUSE_USER_CREDENTIALS);
            return Collections.enumeration(auths);
        }
        return super.getHeaders(name);
    }

    @Override
    public Enumeration getHeaderNames() {
        if (super.getHeader(HttpHeaders.AUTHORIZATION) != null) {
            return super.getHeaderNames();
        }
        List<String> names = Collections.list(super.getHeaderNames());
        names.add(HttpHeaders.AUTHORIZATION);
        return Collections.enumeration(names);
    }
}
