/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.model;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.List;
import javax.json.bind.annotation.JsonbProperty;
import org.eclipse.microprofile.graphql.Type;

/**
 * @author Dmitry Repchevsky
 */

@Type("Tool")
public class Tool {
    private String id;
    private Integer revision;
    private String schema;
    private String original_id;
    private URI registry_tool_id;
    private String name;
    private String description;
    private Boolean automated;
    private String status;
    private ZonedDateTime activation;
    private ZonedDateTime deactivation;
    
    private ZonedDateTime created;
    private ZonedDateTime updated;

    private List<String> community_ids;
    private List<String> tool_contact_ids;
    private List<String> references;
    private List<ToolAccess> tool_access;

    public Tool() {}

    @JsonbProperty("_id")
    public String getID() {
        return id;
    }
    
    @JsonbProperty("_id")
    public void setID(String id) {
        this.id = id;
    }

    @JsonbProperty("_revision")
    public Integer getRevision() {
        return revision;
    }
    
    @JsonbProperty("_revision")
    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    @JsonbProperty("_schema")
    public String getSchema() {
        return schema;
    }
    
    @JsonbProperty("_schema")
    public void setSchema(String schema) {
        this.schema = schema;
    }
    
    @JsonbProperty("orig_id")
    public String getOriginalID() {
        return original_id;
    }
    
    @JsonbProperty("orig_id")
    public void setOriginalID(String original_id) {
        this.original_id = original_id;
    }

    @JsonbProperty("registry_tool_id")
    public URI getRegistryToolID() {
        return registry_tool_id;
    }
    
    @JsonbProperty("registry_tool_id")
    public void setRegistryToolID(URI registry_tool_id) {
        this.registry_tool_id = registry_tool_id;
    }
    
    @JsonbProperty("name")
    public String getName() {
        return name;
    }
    
    @JsonbProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonbProperty("description")
    public String getDescription() {
        return description;
    }
    
    @JsonbProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }
    
    @JsonbProperty("is_automated")
    public Boolean getAutomated() {
        return automated;
    }
    
    @JsonbProperty("is_automated")
    public void setAutomated(Boolean automated) {
        this.automated = automated;
    }
    
    @JsonbProperty("status")
    public String getStatus() {
        return status;
    }
    
    @JsonbProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }
    
    @JsonbProperty("activation")
    public ZonedDateTime getActivation() {
        return activation;
    }
    
    @JsonbProperty("activation")
    public void setActivation(ZonedDateTime activation) {
        this.activation = activation;
    }
    
    @JsonbProperty("deactivation")
    public ZonedDateTime getDeactivation() {
        return deactivation;
    }
    
    @JsonbProperty("deactivation")
    public void setDeactivation(ZonedDateTime deactivation) {
        this.deactivation = deactivation;
    }
    
    @JsonbProperty("_created")
    public ZonedDateTime getCreated() {
        return created;
    }

    @JsonbProperty("_created")
    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    @JsonbProperty("_updated")
    public ZonedDateTime getUpdated() {
        return updated;
    }

    @JsonbProperty("_updated")
    public void setUpdated(ZonedDateTime updated) {
        this.updated = updated;
    }

    @JsonbProperty("community_ids")
    public List<String> getCommunityIDs() {
        return community_ids;
    }
    
    @JsonbProperty("community_ids")
    public void setCommunityIDs(List<String> community_ids) {
        this.community_ids = community_ids;
    }
    
    @JsonbProperty("tool_contact_ids")
    public List<String> getToolContactIDs() {
        return tool_contact_ids;
    }
    
    @JsonbProperty("tool_contact_ids")
    public void setToolContactIDs(List<String> tool_contact_ids) {
        this.tool_contact_ids = tool_contact_ids;
    }
    
    @JsonbProperty("references")
    public List<String> getReferences() {
        return references;
    }
    
    @JsonbProperty("references")
    public void setReferences(List<String> references) {
        this.references = references;
    }
    
    @JsonbProperty("tool_access")
    public List<ToolAccess> getToolAccess() {
        return tool_access;
    }
    
    @JsonbProperty("tool_access")
    public void setToolAccess(List<ToolAccess> tool_access) {
        this.tool_access = tool_access;
    }
}
