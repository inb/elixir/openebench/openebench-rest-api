/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */
package es.bsc.inb.elixir.openebench.graphql.model;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.List;
import javax.json.JsonObject;
import javax.json.bind.annotation.JsonbProperty;
import org.eclipse.microprofile.graphql.Type;

/**
 * @author Dmitry Repchevsky
 */

@Type("Challenge")
public class Challenge {
    private String id;
    private Integer revision;
    private String schema;
    private String original_id;
    private String name;
    private String acronym;
    private String description;
    private JsonObject metadata;
    private String benchmarking_event_id;
    private Boolean automated;
    private ChallengeDates dates;
    private URI url;
    private List<MetricsCategories> metrics_categories;    
    private List<String> challenge_contact_ids;
    private List<String> references;

    private ZonedDateTime created;
    private ZonedDateTime updated;

    private List<Dataset> datasets;
    private List<TestAction> test_actions;

    public Challenge() {}

    @JsonbProperty("_id")
    public String getID() {
        return id;
    }
    
    @JsonbProperty("_id")
    public void setID(String id) {
        this.id = id;
    }

    @JsonbProperty("_revision")
    public Integer getRevision() {
        return revision;
    }
    
    @JsonbProperty("_revision")
    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    @JsonbProperty("_schema")
    public String getSchema() {
        return schema;
    }
    
    @JsonbProperty("_schema")
    public void setSchema(String schema) {
        this.schema = schema;
    }    

    @JsonbProperty("orig_id")
    public String getOriginalID() {
        return original_id;
    }
    
    @JsonbProperty("orig_id")
    public void setOriginalID(String original_id) {
        this.original_id = original_id;
    }

    @JsonbProperty("name")
    public String getName() {
        return name;
    }

    @JsonbProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonbProperty("acronym")
    public String getAcronym() {
        return acronym;
    }
    
    @JsonbProperty("acronym")
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    @JsonbProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonbProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonbProperty("_metadata")
    public JsonObject getMetadata() {
        return metadata;
    }

    @JsonbProperty("_metadata")
    public void setMetadata(JsonObject metadata) {
        this.metadata = metadata;
    }

    @JsonbProperty("benchmarking_event_id")
    public String getBenchmarkingEventID() {
        return benchmarking_event_id;
    }

    @JsonbProperty("benchmarking_event_id")
    public void setBenchmarkingEventID(String benchmarking_event_id) {
        this.benchmarking_event_id = benchmarking_event_id;
    }

    @JsonbProperty("is_automated")
    public Boolean getAutomated() {
        return automated;
    }
    
    @JsonbProperty("is_automated")
    public void setAutomated(Boolean automated) {
        this.automated = automated;
    }

    @JsonbProperty("dates")
    public ChallengeDates getDates() {
        return dates;
    }

    @JsonbProperty("dates")
    public void setDates(ChallengeDates dates) {
        this.dates = dates;
    }

    @JsonbProperty("url")
    public URI getURL() {
        return url;
    }

    @JsonbProperty("url")
    public void setURL(URI url) {
        this.url = url;
    }

    @JsonbProperty("metrics_categories")
    public List<MetricsCategories> getMetricsCategories() {
        return metrics_categories;
    }

    @JsonbProperty("metrics_categories")
    public void setMetricsCategories(List<MetricsCategories> metrics_categories) {
        this.metrics_categories = metrics_categories;
    }

    @JsonbProperty("challenge_contact_ids")
    public List<String> getChallengeContactIDs() {
        return challenge_contact_ids;
    }

    @JsonbProperty("challenge_contact_ids")
    public void setChallengeContactIDs(List<String> challenge_contact_ids) {
        this.challenge_contact_ids = challenge_contact_ids;
    }

    @JsonbProperty("references")
    public List<String> getReferences() {
        return references;
    }

    @JsonbProperty("references")
    public void setReferences(List<String> references) {
        this.references = references;
    }

    @JsonbProperty("_created")
    public ZonedDateTime getCreated() {
        return created;
    }

    @JsonbProperty("_created")
    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    @JsonbProperty("_updated")
    public ZonedDateTime getUpdated() {
        return updated;
    }

    @JsonbProperty("_updated")
    public void setUpdated(ZonedDateTime updated) {
        this.updated = updated;
    }

    @JsonbProperty("datasets")
    public List<Dataset> getDatasets() {
        return datasets;
    }

    @JsonbProperty("datasets")
    public void setDatasets(List<Dataset> datasets) {
        this.datasets = datasets;
    }
    
    @JsonbProperty("test_actions")
    public List<TestAction> getTestActions() {
        return test_actions;
    }

    @JsonbProperty("test_actions")
    public void setTestActions(List<TestAction> test_actions) {
        this.test_actions = test_actions;
    }
}
