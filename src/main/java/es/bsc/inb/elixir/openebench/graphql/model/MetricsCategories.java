/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.model;

import java.util.List;
import javax.json.bind.annotation.JsonbProperty;
import org.eclipse.microprofile.graphql.Type;

/**
 * @author Dmitry Repchevsky
 */

@Type("MetricsCategory")
public class MetricsCategories {
    private String category;
    private String description;
    private List<ToolMetrics> metrics;

    public MetricsCategories() {}

    @JsonbProperty("category")
    public String getCategory() {
        return category;
    }

    @JsonbProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    @JsonbProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonbProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonbProperty("metrics")
    public List<ToolMetrics> getMetrics() {
        return metrics;
    }

    @JsonbProperty("metrics")
    public void setMetrics(List<ToolMetrics> metrics) {
        this.metrics = metrics;
    }
}
