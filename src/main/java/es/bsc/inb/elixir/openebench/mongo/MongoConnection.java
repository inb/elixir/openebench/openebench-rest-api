/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.mongo;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.ServletContext;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class MongoConnection {
    
    @Inject 
    private ServletContext ctx;

    private MongoClient staged_client;
    private MongoClient sandbox_client;

    private MongoDatabase staged_database;
    private MongoDatabase sandbox_database;
    
    public MongoDatabase getStagedDatabase() {
        return staged_database;
    }

    public MongoDatabase getSandboxDatabase() {
        return sandbox_database;
    }
    
    @PostConstruct
    public void init() {

        final String staged_url = ctx.getInitParameter("mongodb.staged.url");
        if (staged_url == null || staged_url.isEmpty()) {
            Logger.getLogger(MongoConnection.class.getName()).log(Level.SEVERE, "no 'mongodb.staged.url' mongodb url defined");
            return;
        }

        final String sandbox_url = ctx.getInitParameter("mongodb.sandbox.url");
        if (sandbox_url == null || sandbox_url.isEmpty()) {
            Logger.getLogger(MongoConnection.class.getName()).log(Level.SEVERE, "no 'mongodb.sandbox.url' mongodb url defined");
            return;
        }

        try {
            final ConnectionString staged_con = new ConnectionString(staged_url);
            final ConnectionString sandbox_con = new ConnectionString(sandbox_url);

            staged_client = MongoClients.create(staged_con);
            sandbox_client = MongoClients.create(sandbox_con);

            staged_database = staged_client.getDatabase(staged_con.getDatabase());
            sandbox_database = sandbox_client.getDatabase(sandbox_con.getDatabase());
        } catch (Exception ex) {
            Logger.getLogger(MongoConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @PreDestroy
    public void destroy() {
        staged_client.close();
        sandbox_client.close();
    }
}
