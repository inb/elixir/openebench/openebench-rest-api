/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.inputs;

import es.bsc.inb.elixir.openebench.graphql.CompositeID;
import javax.json.bind.annotation.JsonbProperty;
import org.eclipse.microprofile.graphql.Input;

/**
 * @author Dmitry Repchevsky
 */

@Input("DatasetFilters")
public class DatasetFilter {
    
    private String id;
    private String visibility;
    private String community_id;
    private String type;
    private String challenge_id;
    private String original_id;
    
    @JsonbProperty("_id")
    public CompositeID getID() {
        return id == null ? null : new CompositeID(id);
    }

    @JsonbProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    @JsonbProperty("community_ids")
    public String getCommunityID() {
        return community_id;
    }

    @JsonbProperty("community_id")
    public void setCommunityId(String community_id) {
        this.community_id = community_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonbProperty("challenge_ids")
    public String getChallengeID() {
        return challenge_id;
    }

    @JsonbProperty("challenge_id")
    public void setChallengeId(String challenge_id) {
        this.challenge_id = challenge_id;
    }
    
    @JsonbProperty("orig_id")
    public String getOriginalId() {
        return original_id;
    }

    @JsonbProperty("orig_id")
    public void setOriginalId(String original_id) {
        this.original_id = original_id;
    }
}
