/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.security;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.security.enterprise.AuthenticationStatus;
import javax.security.enterprise.SecurityContext;
import javax.security.enterprise.authentication.mechanism.http.AuthenticationParameters;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.wildfly.security.http.oidc.AccessToken;
import org.wildfly.security.http.oidc.OidcPrincipal;
import org.wildfly.security.http.oidc.OidcSecurityContext;
import org.wildfly.security.http.oidc.RealmAccessClaim;

/**
 * @author Dmitry Repchevsky
 */

public class OEBSecurityContext implements SecurityContext {

    public final static String ROLES_CLAIM = "oeb:roles";

    private final List<String> oeb_roles;
    private final javax.ws.rs.core.SecurityContext ctx;

    public OEBSecurityContext(OidcPrincipal principal) {
        ctx = null;
        oeb_roles = getOEBRoles(principal);
    }

    public OEBSecurityContext(javax.ws.rs.core.SecurityContext ctx) {
        this.ctx = ctx;
        if (ctx == null) {
            oeb_roles = Collections.EMPTY_LIST;
        } else {
            final Principal principal = ctx.getUserPrincipal();
            if (principal instanceof OidcPrincipal) {
                oeb_roles = getOEBRoles((OidcPrincipal)principal);
            } else {
                oeb_roles = Collections.EMPTY_LIST;
            }
        }
    }

    private List<String> getOEBRoles(OidcPrincipal principal) {
        final ArrayList<String> list = new ArrayList();
        
        final OidcPrincipal oidc_principal = (OidcPrincipal)principal;
        final OidcSecurityContext oidc = oidc_principal.getOidcSecurityContext();
        final AccessToken token = oidc.getToken();
        
        List<String> roles = token.getClaimValue(ROLES_CLAIM, List.class);
        if (roles != null) {
            list.addAll(roles);
        }
        
        final RealmAccessClaim claim = token.getRealmAccessClaim();
        if (claim != null) {
            roles = claim.getRoles();
            if (roles != null) {
               list.addAll(roles);
            }
        }
        
        return list;
    }
    
    @Override
    public Principal getCallerPrincipal() {
        return ctx == null ? null : ctx.getUserPrincipal();
    }

    @Override
    public <T extends Principal> Set<T> getPrincipalsByType(Class<T> type) {
        if (ctx != null) {
            final Principal principal = ctx.getUserPrincipal();
            if (principal != null && principal.getClass().isAssignableFrom(type)){
                return Set.of((T)principal);
            }
        }
        return Collections.EMPTY_SET;
    }

    @Override
    public boolean isCallerInRole(String role) {
        if (oeb_roles.contains(role) || (ctx != null && ctx.isUserInRole(role))) {
            return true;
        }
        
        // special 'wildcard' for partial match, for instance, isCallerInRole('manager') 
        for (String oeb_role : oeb_roles) {
            if (oeb_role != null && oeb_role.startsWith(role)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean hasAccessToWebResource(String string, String... strings) {
        return false;
    }

    @Override
    public AuthenticationStatus authenticate(HttpServletRequest req, HttpServletResponse res, AuthenticationParameters p) {
        return null;
    }    
}
