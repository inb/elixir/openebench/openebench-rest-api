/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.model;

import java.time.ZonedDateTime;
import javax.json.bind.annotation.JsonbProperty;
import org.eclipse.microprofile.graphql.Type;

/**
 * @author Dmitry Repchevsky
 */

@Type("CDates")
public class ChallengeDates  extends Dates {
    private ZonedDateTime challenge_start;
    private ZonedDateTime challenge_stop;

    public ChallengeDates() {}

    @JsonbProperty("challenge_start")
    public ZonedDateTime getChallengeStart() {
        return challenge_start;
    }

    @JsonbProperty("challenge_start")
    public void setChallengeStart(ZonedDateTime challenge_start) {
        this.challenge_start = challenge_start;
    }

    @JsonbProperty("challenge_stop")
    public ZonedDateTime getChallengeStop() {
        return challenge_stop;
    }

    @JsonbProperty("challenge_stop")
    public void setChallengeStop(ZonedDateTime challenge_stop) {
        this.challenge_stop = challenge_stop;
    }
}
