/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.model;

import java.net.URI;
import java.util.List;
import javax.json.bind.annotation.JsonbProperty;
import org.eclipse.microprofile.graphql.Type;

/**
 * @author Dmitry Repchevsky
 */

@Type("Reference")
public class Reference {
    private String id;
    private String schema;
    private String title;
    private List<URI> bibliographic_ids;
    private String _abstract;
    private List<Author> authors;
    
    public Reference() {}

    @JsonbProperty("_id")
    public String getID() {
        return id;
    }
    
    @JsonbProperty("_id")
    public void setID(String id) {
        this.id = id;
    }
    
    @JsonbProperty("_schema")
    public String getSchema() {
        return schema;
    }
    
    @JsonbProperty("_schema")
    public void setSchema(String schema) {
        this.schema = schema;
    }  
    
    @JsonbProperty("title")
    public String getTitle() {
        return title;
    }
    
    @JsonbProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }
    
    @JsonbProperty("bibliographic_ids")
    public List<URI> getBibliographicIDs() {
        return bibliographic_ids;
    }
    
    @JsonbProperty("bibliographic_ids")
    public void setBibliographicIDs(List<URI> bibliographic_ids) {
        this.bibliographic_ids = bibliographic_ids;
    }
    
    @JsonbProperty("abstract")
    public String getAbstract() {
        return _abstract;
    }
    
    @JsonbProperty("abstract")
    public void setAbstract(String _abstract) {
        this._abstract = _abstract;
    }
    
    @JsonbProperty("authors")
    public List<Author> getAuthors() {
        return authors;
    }
    
    @JsonbProperty("authors")
    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }
}
