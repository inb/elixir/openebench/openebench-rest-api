/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.graphql.model;

import java.net.URI;
import java.time.ZonedDateTime;
import javax.json.bind.annotation.JsonbProperty;
import org.eclipse.microprofile.graphql.Type;

/**
 * @author Dmitry Repchevsky
 */

@Type("IdSolve")
public class IdSolve {
    private String id;
    private String schema;
    private String description;
    private String pattern;
    private URI endpoint;

    private ZonedDateTime created;
    private ZonedDateTime updated;

    public IdSolve() {}

    @JsonbProperty("_id")
    public String getID() {
        return id;
    }
    
    @JsonbProperty("_id")
    public void setID(String id) {
        this.id = id;
    }
    
    @JsonbProperty("_schema")
    public String getSchema() {
        return schema;
    }
    
    @JsonbProperty("_schema")
    public void setSchema(String schema) {
        this.schema = schema;
    }

    @JsonbProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonbProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }
    
    @JsonbProperty("pattern")
    public String getPattern() {
        return pattern;
    }

    @JsonbProperty("pattern")
    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
    
    @JsonbProperty("endpoint")
    public URI getEndpoint() {
        return endpoint;
    }

    @JsonbProperty("endpoint")
    public void setEndpoint(URI endpoint) {
        this.endpoint = endpoint;
    }
    
    @JsonbProperty("_created")
    public ZonedDateTime getCreated() {
        return created;
    }

    @JsonbProperty("_created")
    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    @JsonbProperty("_updated")
    public ZonedDateTime getUpdated() {
        return updated;
    }

    @JsonbProperty("_updated")
    public void setUpdated(ZonedDateTime updated) {
        this.updated = updated;
    }
}
