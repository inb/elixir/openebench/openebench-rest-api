package es.bsc.inb.elixir.openebench.mongo;

import es.bsc.inb.elixir.openebench.application.OEBSchemaLocatorConfig;
import es.bsc.inb.elixir.openebench.auth.Authorization;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class DAOProducer {

    @Inject
    private OEBSchemaLocatorConfig locator;
    
    @Inject
    private MongoConnection mongo;

    private OpenEBenchDAO sandbox;
    private OpenEBenchDAO staged;

    @PostConstruct
    public void init() {    
        sandbox = new OpenEBenchDAO(
            new Authorization(mongo.getStagedDatabase(), mongo.getSandboxDatabase()),
            locator.getSchemaResolver());

        staged = new OpenEBenchDAO(
                new Authorization(mongo.getStagedDatabase(), null),
                locator.getSchemaResolver());
    }

    @Produces
    @Named("sandbox")
    public OpenEBenchDAO getSandboxDAO() {
        return sandbox;
    }

    @Produces
    @Named("staged")
    public OpenEBenchDAO getStagedDAO() {
        return staged;
    }

}
