###  OpenEBench REST API for benchmarking data

###### Enterprise Java 8 (JEE8) Platform
OpenEBench REST API is strictly adherent to JEE8 specification.
The API is developed and deployed on [WildFly 17.1](http://wildfly.org/) server, 
but should run on other servers (i.e. [Apache Tomcat](http://tomcat.apache.org/)).

###### MongoDB
The data is stored in [MongoDB](www.mongodb.com)

###### Apache Maven build system
To simplify build process API uses [Apache Maven](https://maven.apache.org/) build system.

To build OpenEBench REST application:

Compiling:
```shell
>git clone https://gitlab.bsc.es/inb/elixir/openebench/openebench-rest-api.git
>cd openebench-rest-api
>mvn install
```

##### REST API

- __GET__ /{collection} ```ACCEPT: application/json``` return an array of all accessible collection data objects
- __GET__ /{collection} ```ACCEPT: text/uri-list``` returns a plain list of data identifiers found in the collection
---
- __GET__ /{collection}/{id} return data object reffered by its id
- __HEAD__ /{collection}/{id} check whether the object exists

##### SECURITY

OpenEBench REST API filters output according permissions assigned to the authenticated user.
If no authentication (OpenID Connect token) data provided, only publicly available data is returned.
